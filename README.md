# MuttsWorld Guide to Moderating #

## For Creative Mods ##

Last updated Jan. 2016

## By DisturbdGuy and XXLordSXX ##

So, you’ve been promoted to a staff position with the MuttsWorld gaming community. Or maybe you’re a veteran staffer just visiting this document for a refresher. Either way, settle in! This guide will get you up to speed on how you can help keep MuttsWorld one of the best gaming experiences around.

**Section one: Behavior**

The first and most important part of being staff, whether a new ambassador or a veteran moderator, is knowing that you represent the whole of MuttsWorld. We expect you to:

1)	Be friendly

2)	Be involved

3)	Be professional

4)	Be. Professional. – Yes, this is listed twice.

*Being friendly*: This above all. Say hey to new players and ask if they have any questions. Say hey to returning players when they sign back in. Join in the conversation in public chats and answer questions freely. When you are friendly with the players, they want to stick around and feel like they belong. The longer they stick around and the more they feel that this is “their” server, the more likely they are to donate. The more likely they are to donate, the longer we can keep being an awesome server (servers cost money).

*Being involved*: If a player is proud of something they’ve built or done, ask to see it. Encourage and participate with players in the Buildgame. You might be surprised at how much fun this can be. Use /tpnext and /tpprev to tp to players randomly. Most of the time they will appreciate the fact that you took the time to visit their plot. Use disguises to play around with players! While this can be considered trolling, most players will have fun with you doing this. If they get upset, just move on.  Drop into a player’s head shop or skin competition. Players love seeing staff getting involved with these.

*Being professional* is the hardest part of being staff. Many of our players are young, and the ones who aren’t young, some of them are trolls. One trait that young players and trolls have in common? They are absolutely certain that they shouldn’t be in trouble, and when someone says they are, they either get upset or mad. If they decide they don’t like you, they’ll find reasons to make that known in as public a way as they can. All while they continue to expect you to save them from rule-breakers. It can be a challenge to stay nice and professional with these ne’er-do-wells, but that is a challenge you must meet head-on. If you’re having trouble dealing with someone, ask another staff member to sub in for you in staff chat.

Being professional isn’t just about how you deal with problem players. It’s also in how you interact with your fellow staff. We are a diverse group of people, with different personalities and interests. You’re bound to butt heads with someone eventually. DO NOT EVER do so on the server in any public chat. You are to try to work out the problem with the person directly first, in staff chat if you need a second opinion or using a senior staff member as a mediator if neither of those two options work. 
If you are incapable or unwilling to follow these simple guidelines, you may wish to ask an admin to demote you. We don’t mind having you play on our servers, but we have to make sure we have only the best people on staff.

**Section two: Communication**

You’ll notice in the requirements for becoming staff that we include things like IRC, TS and the Forums. I’m going to add one more you need to know: Slack. These are not suggestions; they are mandatory for all staff. They help us keep in touch not online with the players, but with each other to share important staff information. A brief bit on each is below:

*Forums* – To be staff, you had to register here and post an application. To be GOOD staff, you should continue using and checking in on the forums to communicate with players, check out problems, see new ideas, etc. There’s even a staff-only section of the forums (but you’ll have to ask mumblerit for access).

*TS (TeamSpeak)* – TeamSpeak is another chat client, but this one allows for voice. You can hear your fellow staff and other players banter on… and on… and on. Don’t worry, though. There’s a staff channel if you need a little quiet time. Like IRC, there are several clients that’ll do the job, but we recommend TeamSpeak 3 (look it up). Like the forums, you want to be here to Be Friendly and Be Involved with players and other staff. But unlike forums, this is also where we hold important staff meetings to discuss community changes or problems.

*Slack* – Unlike our previous staff chat solution, Slack has the benefit of being browser-based, so you don’t have to download anything. Also, only staff members have access to our channels, allowing us to freely and instantly discuss important problems, changes or concerns. Our team currently has separate channels for each of our servers, as well as a General chat for all-community discussion. As of this writing, Slack is only offered to Moderators and higher. 

*Appeals* – As a mod, you are responsible for communicating with the players you’ve banned through appeals. When mumblerit has set you up with appeal access, you can access it on the website either by clicking on the “Admin CP” button on the top left of the home page (or by following this link: https://www.muttsworldmine.com/forums/admin/index.php?app=nexus&module=tickets&section=list ) You are expected to complete appeals in a quick and timely manner at your earliest convenience so that we can get players back on the server. If you see there is an appeal for you, you can use one of the template responses or write your own. Use your best judgment. If it is a first offense, give them the benefit of the doubt unless they’ve committed a truly heinous crime and show absolutely no remorse. At the end of the day, it’s up to you and we trust in your ability to be fair. The fates of our players rest in your hands so be fair, be efficient, be judicious. Remember to assign your appeals to yourself, working is for appeals that you would them to respond to, closed is for denied appeals, resolved is for successful appeals. Then go on the server and /unban the player. In addition, if you see there is an appeal for someone else that has not been addressed in a reasonable amount of time (this is where signing your bans comes in handy *wink wink*) you may see fit to send them a reminder message via slack. 

**Section three: Basic commands**

The first thing you’ll want is a firm grasp on WorldEdit commands. You can look up those command lists, and others, online. WE commands will be some of your most-used tools in moderating, though I would not consider them the most important. Other commands vary depending on what server you are on, but these are the absolute basics. Not sure of something? DON’T EXPERIMENT! Ask another staffer first. 

[] = Mandatory				() = Optional

*The Big Ones* – You better have a VERY good reason to use these!

- /kick [playername] (reason) – This command temporarily removes a player from the server without banning them. Mainly used as a last warning to a player that is repeatedly causing problems, but not big enough to deserve a ban.

- /ban [playername] [reason] [banning staff] Appeal at bit.ly/MWAppeal – Absolutely last resort solution to a problem player, who has caused so much damage that it is obviously not an accident or misunderstanding. YOU MUST include the reason they were banned, your name or initials and where they can appeal. 

- /baninfo [playername] – Allows you to see the /ban information on players that were banned by other mods.

- /rts ban [playername] – Bans players from making modreqs. There’s no time limit on these bans and no appeal. Don’t forget you used this on a player, you may see it fit to unban them later. Only use this in the case of players spamming modreqs despite your warnings, or players abusing them in other ways with vulgar language. 

*Legendchat* - Because they sure do love to talk. Excessive cursing/harassing other players/rudeness/spam/advertising/etc. Use common sense.

- /mute [playername] [amount of time in minutes] – Example: /mute disturbdguy 5 will shut me up for 5 minutes. Only use this after warning the player. (Full command: /lc mute [playername] [time in minutes])

- /fl [playername] OR /lc playerch [playername] l – Forces the player into Local chat, if they’re misusing a channel or talking to someone near them in global. Only use after asking the player to use the right channel.

*LogBlock* – You won’t have to use this particularly often on creative, but you can use it to check grief and who built things. If you’re not sure about a command ALWAYS ask another staff member.

- /lb rollback area [distance] player [name] block [ID or name without spaces] time [number] – EASILY the most dangerous command in the staff arsenal. NEVER use it without the limiting parameters (area, player, block, etc.) and ALWAYS check to see if the block count makes sense. After the time [number], be sure to include whether that is in minutes, hours or days. The block limiter can be left out, if you wish to roll back all blocks.

- /lb redo area [distance] player [name] block [ID or name without spaces] time [number] – The opposite of rollback, this command will bring back blocks. We don’t use this one too much, unless there has been a server glitch, rollback or a banned player successfully appealing.

- /lbp [playername] – Brings up a helpful list of the blocks the player has created or destroyed. The blocks most commonly broken or created show up higher on the list, and you may have to use the next command to see blocks that are out of the top 15 or so.

- /lb player [playername] area [number] block [ID or name] time [number] (sum blocks) – A more precise version of LBP. It will bring up recent block changes from the player. “sum blocks” will bring up a number tally instead. You can remove any of the limiters. For instance, without “player playername” or “block id” the command will return all block changes in the area.

**Section four: Other commands**

- /p setowner [owner]  - Sets the owner for a plot

- /p set flag [flag]  - Sets a worldguard flag on a plot. Mods can set flags but can't delete them.

- /marry – Lists marriage commands

- /ret – Short for Return. Teleports you to the place you last were. 

- /tp [player 1] [player 2] - Teleports player 1 to player 2

- /bring [player] - Teleports the player to you.

- /nv – Gives you nightvision

- /give [item] [player] – Gives the player the item you specify

- /dis [mob] – Disguises you as a mob.

- /displayer [player] [mob] – Disguises player as mob.

- /dis player [player] - Disguises you as another player. 

- /displayer [player] player [player] - Disguises another player with the player you specify.

There are also other commands you can explore and find, including more disguises.

**Creative specific things:**

*Bugged plots*: Sometimes you will get a modreq or have a player stating that they cannot build in a certain place on their merged plot. This is because the plot has become unlinked and it’s behaving as if the player is trying to build in the road area. To fix this, you will usually need to merge the plot, unlink it, and merge it again. Do your best to copy or move the players build if it will be affected by unlinking the plots.

*Inappropriate builds*: This can happen on about any of our servers, but Creative will see more of it. No adult themes such as Strip Clubs are ever acceptable. Hate-filled themes or messages including signs are not tolerated either. As a Mod, your judgement is trusted on these matters. You are not required to screenshot a questionable build, but you may wish to if you need a second opinion. If you catch the player in the act of building something inappropriate, ask them to stop. If they refuse or ignore you, ban them and clear the plot. If you find a build that is really heinous and the player is offline, just ban them and clear the plot as well.

*Redstone/laggy builds*: Any build that causes consistent lag for others needs to be removed. This is usually not a bannable offense unless the player is persistent in building such things. We allow redstone clocks, but they should be kept to a low repeat rate and have a switch to turn them off. If you find a running clock, either fix it with a switch to turn it off, or disable it and leave a sign asking the player to do add a switch themselves. Redstone clocks that serve no function other than to cause lag should be removed as well... Example: a Plot where you see 50 pistons and or doors constantly running. These are nothing more than lag machines. 

*Marriages*: This should be familiar from your job as an ambassador. The same responsibilities apply.

**A note on hackers**

The creative rules for banning hackers are as follows:

1. If they're super speed, that’s obvious. Don't hesitate to ban.

2. If they have hacked or illegal items, contact an admin immediately. Some of these items can break the server if used in malicious ways. Investigate the source of these items and ban the person who created them.

3. The hacked items are typically found in the form of books or potions, but also extend to enchanted items such as boots. Always inform an admin of these types of hacks. 
	
Lastly, if you have any questions, consult the “**stafftrainingsquad**” slack channel. If you are not in this channel, ask to join it. This is the place for all new mods to learn more about their job.

### Thank you for your time and see you on the server! ###